# DEV Dotfiles

> everything I need for development related work

These configs is meant to be as OS agnostic as possible

Configuration related to window management, app-launching etc do not belong here.

> This repo is ideal to be used as a submodule in the desktop dotfile repo

