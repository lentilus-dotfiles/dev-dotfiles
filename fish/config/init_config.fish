#!/bin/fish

set DIR (dirname (status --current-filename))
set CONFIG $DIR/config/configuration
set COMPUTER "generic"
set -gx TERM "screen-256color"

set -gx PATH $PATH /usr/sbin 
set -gx PATH $PATH ~/.local/bin 
set -gx PATH $PATH ~/git/scripts

set EDITOR nvim
set VISUAL nvim
set fish_greeting

source $CONFIG/env.fish
source $CONFIG/xdg.fish
source $CONFIG/python.fish
source $CONFIG/aliases.fish
source $CONFIG/prompt.fish
source $CONFIG/clean_home.fish
source $CONFIG/fisher.fish


test $COMPUTER = "personal" && source $CONFIG/personal.fish
