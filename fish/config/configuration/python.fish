#!/bin/fish

## pyenv
set -gx PYENV_ROOT $HOME/.local/pyenv
set PATH $PYENV_ROOT/bin $PATH

which pyenv &> /dev/null; or git clone https://github.com/pyenv/pyenv.git $HOME/.local/pyenv

pyenv init - --no-rehash fish | source
and sh -c 'command pyenv rehash 2>/dev/null &'

## pipx
set -gx PIPX_DEFAULT_PYTHON $PYENV_ROOT/versions/3.10.10/bin/python
