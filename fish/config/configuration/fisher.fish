#!/bin/fish

set -q INSTALLING_FISHER && exit

if not functions -q fisher
	set -lx INSTALLING_FISHER
	curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source \
		&& fisher install jorgebucaran/fisher \
		&& fisher install mattgreen/lucid.fish
end

