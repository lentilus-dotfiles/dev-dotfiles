# personal environment specific setup

source $HOME/git/note_manager/fish/note_manager.fish
source $HOME/git/xettelkasten/fish/xettelkasten.fish

(status is-interactive); or exit

set -gx GPG_TTY (tty)

(test (tty) = "/dev/tty1"); or exit

# things to start at tty-login
eval (ssh-agent -c)
gpg-agent &
sway
