# echo "cleaning home"
set -gx ANDROID_HOME $XDG_DATA_HOME/android
set -gx CARGO_HOME $XDG_DATA_HOME/cargo
set -gx RUSTUP_HOME $XDG_DATA_HOME/rustup
set -gx HISTFILE $XDG_STATE_HOME/bash/history
set -gx DOCKER_CONFIG $XDG_CONFIG_HOME/docker
set -gx MBSYNCRC $XDG_CONFIG_HOME/isync/mbsyncrc
set -gx PASSWORD_STORE_DIR $HOME/git/password-store
set -gx W3M_DIR $XDG_DATA_HOME/w3m

alias adb='HOME="$XDG_DATA_HOME"/android adb'
alias wget='wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'
alias mbsync='mbsync -c "$XDG_CONFIG_HOME"/isync/mbsyncrc'
