# aliases
# alias dbe="distrobox enter --additional-flags '--env TERM=xterm-256color' ubuntu_test"
alias stow="stow --target=$HOME -v"
alias jt="jump-tmux"
# alias ls="eza"
alias vi="nvim"
alias nnvim='NVIM_APPNAME=nvim_dev nvim'
