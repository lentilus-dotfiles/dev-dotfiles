# Fish Config

the actual config is located in ./config
Fish totally pollutes the config directory so everything except for:

- ./config.fish
- ./config
- ./config/configuration

is in the gitignore.
